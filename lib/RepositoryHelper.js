const gitRepoName = require('git-repo-name');
const gitBranch = require('git-branch');
const { Bitbucket } = require('bitbucket');

/**
 * repository helper for managing connection to bitbucket repo
 */
class RepositoryHelper {
    /**
     * @param {*} workspace This can either be the workspace ID (slug) or the workspace UUID surrounded by curly-braces, for example: {workspace UUID}.
     * @param {*} repo_slug This can either be the repository slug or the UUID of the repository, surrounded by curly-braces, for example: {repository UUID}
     */
    constructor(workspace, repo_slug = undefined) {
        this.workspace = workspace;
        this.repo_slug = repo_slug ? repo_slug : RepositoryHelper.getCurrentRepoName();

        this.clientOptions = {
            baseUrl: 'https://api.bitbucket.org/2.0'
        }

        return this;
    }

    /**
     * set api credentials
     * @param {*} username bitbucket username/alias
     * @param {*} password create App Password for this account (username). See https://bitbucket.org/account/settings/
     */
    setCredentials(username, password) {
        this.clientOptions.auth = {
            username, password
        }

        return this;
    }

    /**
     * init object
     */
    init() {
        this.bitbucket = new Bitbucket(this.clientOptions);

        return this;
    }

    /**
     * get webhooks list for repository
     */
    async listWebhooks() {
        // call bitbucket api for listWebhooks function
        const { data } = await this.bitbucket.repositories.listWebhooks({ workspace: this.workspace, repo_slug: this.repo_slug });

        // list of webhooks will be stored on data.values;
        const { values } = data;

        return values;
    }

    /**
     * create webhook for the repository
     * @param string description webhook description or Title
     * @param string url url that will be triggered by webhook when retrieving registered event for this webhook
     * @param boolean active set active webhook
     * @param array<string> events list of registered event for the webhook
     */
    async createWebhook(description, url, active = true, events = ['repo:push']) {
        // generate payload for api
        const payload = {
            description, url, active, events,
        }

        // https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Bworkspace%7D/%7Brepo_slug%7D/hooks#post
        // call bitbucket api for listWebhooks function
        const result = await this.bitbucket.repositories.createWebhook({ _body: payload, workspace: this.workspace, repo_slug: this.repo_slug });

        return result;
    }

    /**
     * get current git repository name for this project
     */
    static getCurrentRepoName() {
        return gitRepoName.sync();
    }

    /**
     * get current/active branch name (git repository) for this project
     */
    static getCurrentGitBranch() {
        return gitBranch.sync();
    }
}

module.exports = RepositoryHelper;
