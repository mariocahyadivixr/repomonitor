const repoWebhookSetup = require('./repoWebhookSetup');
const { log } = require('./logger');
const { webhookConfig } = require('../config');

module.exports = {
  /**
   * init repository monitor
   * @param {*} app express app
   */
  init: async (app) => {
    try {
      // register webhook for this repository
      await repoWebhookSetup.registerWebhook();

      // create express route for webhook handler
      app.post(`/${webhookConfig.routePath}`, (req, res) => {
        log.info(`${webhookConfig.routePath} is called`);
      })
    } catch (err) {
        logger.error(err);
    }
  },
};
