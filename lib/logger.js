const bunyan = require('bunyan');
const fs = require('fs');
const path = require('path');

const config = require('../config');

/**
 * USAGE
 * 1. initialiaze on main script
 *
 * const log = require('./lib/logger').init('instance-name');
 *
 * 2. import on any script that need logger
 *
 * const { log } = require('./lib/logger);
 */

// store log instance
let log = {
  fatal: (params) => { console.log(params); },
  warn: (params) => { console.log(params); },
  error: (params) => { console.error(params); },
  info: (params) => { console.info(params); },
  debug: (params) => { console.log(params); },
  trace: (params) => { console.log(params); },
};

// : 'advanced-forecast-server'
module.exports = {
  get log() {
    return log;
  },
  init: (name = 'default') => {
    // get log level from configuration or default 'info'
    const logLevel = config.logLevel || 'info';

    // get logs folder path
    const logFolder = path.join(config.rootPath, 'logs');

    // check if logs folder is exist, IF not then create it
    if (!fs.existsSync(logFolder)) {
      fs.mkdirSync(logFolder, { recursive: true });
    }

    // get log filename, replace space with -
    const logFilename = `${name.replace(/\s+/g, '-')}.log`;
    const logPath = path.join(logFolder, logFilename);

    // create bunyan logger instance
    log = bunyan.createLogger({
      name,
      streams: [
        {
          level: 'debug',
          stream: process.stdout, // log INFO and above to stdout
        },
        {
          level: logLevel,
          path: logPath, // log LEVEL and above to a file
        },
      ],

      // serialize error
      err: bunyan.stdSerializers.err,
    });

    return log;
  },
};
