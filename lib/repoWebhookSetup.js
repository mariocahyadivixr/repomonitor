const _ = require('lodash');
const { log } = require('./logger');
const RepositoryHelper = require('./RepositoryHelper');

const { webhookConfig, baseUrl, username, password } = require('../config');

// create repository helper
const repositoryHelper = new RepositoryHelper(username)
    .setCredentials(username, password)
    .init();

// this will be webhook url for this repository for handling repo events
const webhookUrl = `${baseUrl}/${webhookConfig.routePath}`;

/**
 * register webhook if not exists
 */
const registerWebhook = async () => {
    try {
        // get list of webhooks for this repository
        const webhooks = await repositoryHelper.listWebhooks();

        // get webhooks that match with webhookUrl
        const webhook = _.find(webhooks, { url: webhookUrl });
        console.log(webhook);

        // webhook is not setup yet, create new one
        if (!webhook) {
            // create webhook
            await repositoryHelper.createWebhook(webhookConfig.description, webhookUrl, true, webhookConfig.events);

            console.log(`Webhook ${webhookUrl} is created`);
        }
    } catch (err) {
        log.error(err);
    }
}

module.exports = {
    registerWebhook,
}
