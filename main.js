const express = require('express');
const bodyParser = require('body-parser');

const config = require('./config');

// initializing logger for this API
const log = require('./lib/logger').init('advanced-forecast-server');

const repositoryMonitorSetup = require('./lib/repositoryMonitorSetup');

const app = express();

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

(async () => {
    try {
        // init repository monitor
        await repositoryMonitorSetup.init(app);

        app.get('/', (req, res) => {
            res.send('Hello World!')
        });

        app.listen(config.serverPort, () => {
            console.log(`Listening at http://${config.baseUrl}:${config.serverPort}`)
        });
    } catch (err) {
        log.error(err);
    }
})();