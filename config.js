// get root path for this app
const rootPath = __dirname;

module.exports = {
    username: process.env.USERNAME,
    password: process.env.PASSWORD,
    baseUrl: process.env.BASE_URL,
    serverPort: process.env.SERVER_PORT,
    rootPath,

    webhookConfig: {
        description: 'Repository Monitor Webhook',
        routePath : 'repoPush',
        events: ['repo:push'],
    }
};