// get root path for this app
const rootPath = __dirname;

module.exports = {
    // git workspace
    username: 'username',

    // create App Password for this account (username)
    // https://bitbucket.org/account/settings/
    password: 'password',

    // base url for webhook trigger url
    baseUrl: 'http://site.com',

    // server port
    serverPort: 4000,

    rootPath,

    // webhook configuration
    webhookConfig: {
        // webhook description or Title
        description: 'Repository Monitor Webhook',

        // route path for webhook url and routing
        routePath: 'repoPush',

        // repository events to listen for the webhook
        events: ['repo:push'],
    }
};